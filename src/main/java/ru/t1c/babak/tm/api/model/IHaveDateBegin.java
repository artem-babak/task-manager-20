package ru.t1c.babak.tm.api.model;

import java.util.Date;

public interface IHaveDateBegin {

    Date getDateBegin();

    void setDateBegin(Date dateBegin);

}