package ru.t1c.babak.tm.api.model;

public interface IHaveUserId {

    String getUserId();

    void setUserId(String userId);

}