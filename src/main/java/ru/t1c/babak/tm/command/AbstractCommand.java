package ru.t1c.babak.tm.command;

import ru.t1c.babak.tm.api.model.ICommand;
import ru.t1c.babak.tm.api.model.IWBS;
import ru.t1c.babak.tm.api.service.IAuthService;
import ru.t1c.babak.tm.api.service.IProjectService;
import ru.t1c.babak.tm.api.service.IServiceLocator;
import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.util.DateUtil;

public abstract class AbstractCommand implements ICommand {

    private IServiceLocator serviceLocator;

    public abstract String getName();

    public abstract Role[] getRoles();

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId(){
        return getAuthService().getUserId();
    }

    public abstract String getDescription();

    public String getArgument() {
        return null;
    }

    public abstract void execute();

    protected IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty())
            result += name + " : ";
        if (argument != null && !argument.isEmpty())
            result += argument + " : ";
        if (description != null && !description.isEmpty())
            result += description;
        return result;
    }

    protected void showWbs(final IWBS wbs) {
        System.out.println("\tID: " + wbs.getId());
        System.out.println("\tNAME: " + wbs.getName());
        System.out.println("\tDESCRIPTION: " + wbs.getDescription());
        final Status status = wbs.getStatus();
        if (status != null) System.out.println("\tSTATUS: " + status.getDisplayName());
        System.out.println("\tCREATED: " + DateUtil.toString(wbs.getCreated()));
        System.out.println("\tDATE BEGIN: " + DateUtil.toString(wbs.getDateBegin()));
    }

}
