package ru.t1c.babak.tm.command.project;

import ru.t1c.babak.tm.enumerated.Sort;
import ru.t1c.babak.tm.model.Project;
import ru.t1c.babak.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    public static final String NAME = "project-list";

    public static final String DESCRIPTION = "Show project list.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("\tENTER SORT:");
        System.out.println("\t" + Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId, sort);
        renderProjects(projects);
    }

}
