package ru.t1c.babak.tm.command.project;

import ru.t1c.babak.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-update-by-index";

    public static final String DESCRIPTION = "Update project by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        renderAllProjects();
        System.out.println("\tENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().findOneByIndex(index);
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().updateByIndex(userId, index, name, description);
    }

}
