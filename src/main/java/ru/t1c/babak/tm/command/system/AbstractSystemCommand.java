package ru.t1c.babak.tm.command.system;

import ru.t1c.babak.tm.api.service.ICommandService;
import ru.t1c.babak.tm.command.AbstractCommand;
import ru.t1c.babak.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
