package ru.t1c.babak.tm.command.task;

import ru.t1c.babak.tm.model.Task;
import ru.t1c.babak.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-list-by-project-id";

    public static final String DESCRIPTION = "Show task list which relate to project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("\tENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        renderTasks(tasks);
    }

}
