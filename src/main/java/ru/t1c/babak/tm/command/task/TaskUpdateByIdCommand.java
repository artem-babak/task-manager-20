package ru.t1c.babak.tm.command.task;

import ru.t1c.babak.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-update-by-id";

    public static final String DESCRIPTION = "Update task by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("\tENTER ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().findOneById(id);
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().updateById(userId, id, name, description);
    }

}
