package ru.t1c.babak.tm.command.user;

import ru.t1c.babak.tm.command.AbstractCommand;
import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractCommand {

    public static final String NAME = "login";

    public static final String DESCRIPTION = "Log in.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("\tENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("\tENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getServiceLocator().getAuthService().login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
