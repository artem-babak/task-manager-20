package ru.t1c.babak.tm.command.user;

import ru.t1c.babak.tm.command.AbstractCommand;
import ru.t1c.babak.tm.enumerated.Role;

import java.lang.reflect.Array;
import java.util.Arrays;

public final class UserLogoutCommand extends AbstractCommand {

    public static final String NAME = "logout";

    public static final String DESCRIPTION = "Log out.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        getServiceLocator().getAuthService().logout();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
