package ru.t1c.babak.tm.exception.auth;

import ru.t1c.babak.tm.exception.entity.AbstractEntityNotFoundException;

public final class LoggedInException extends AbstractEntityNotFoundException {

    public LoggedInException() {
        super("Error! You must be logged in...");
    }

}
