package ru.t1c.babak.tm.exception.field;

public final class LoginExistException extends AbstractFieldException {

    public LoginExistException() {
        super("Error! Login already exist...");
    }

}
