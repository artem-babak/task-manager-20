package ru.t1c.babak.tm.exception.field;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! User Id is empty...");
    }

}
