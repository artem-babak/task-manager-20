package ru.t1c.babak.tm.service;

import ru.t1c.babak.tm.api.repository.IRepository;
import ru.t1c.babak.tm.api.service.IService;
import ru.t1c.babak.tm.enumerated.Sort;
import ru.t1c.babak.tm.exception.entity.ModelNotFoundException;
import ru.t1c.babak.tm.exception.entity.ProjectNotFoundException;
import ru.t1c.babak.tm.exception.field.IdEmptyException;
import ru.t1c.babak.tm.exception.field.IndexIncorrectException;
import ru.t1c.babak.tm.model.AbstractModel;
import ru.t1c.babak.tm.model.Project;
import ru.t1c.babak.tm.repository.AbstractRepository;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository){
        this.repository = repository;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public boolean existsById(final String id) {
        if(id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index > repository.getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public M remove(M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.remove(model);
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0 || index > repository.getSize()) throw new IndexIncorrectException();
        final M model = repository.findOneByIndex(index);
        if (model == null) throw new ModelNotFoundException();
        return repository.removeByIndex(index);
    }

    @Override
    public M add(final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @Override
    public void removeAll(Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

}
